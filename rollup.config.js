import nodeResolve from '@rollup/plugin-node-resolve';
import commonJS from '@rollup/plugin-commonjs';

const plugins = [ nodeResolve(), commonJS()];

export default [
  {
    input: "src/index.js",
    plugins,
    output: {
      file: "index.js",
      format: "cjs"
    }
  },
  {
    input: "src/index.js",
    plugins,
    output: {
      file: "index.mjs",
      format: "es"
    }
  }
];
