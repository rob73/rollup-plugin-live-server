rollup-plugin-live-server
=========================

# Why this thing

This will give you a webpack-developer-server experience using the wildly-popular, well-tested [live-server](https://www.npmjs.com/package/live-server).

# Why not rollup-plugin-serve

Because I don't like re-creating the wheel and it doesn't, or I couldn't get it to do, live updates.

# Use

This is a thin wrapper around live-server. You can use all of the options of live-server. If there is a problem with this plugin, it is most likely a problem with live-server.

```
npm i rollup-plugin-live-server -D
```

```Javascript
// rollup.config.js
import {liveServer} from 'rollup-plugin-live-server';
// or
const liveServer = require('rollup-plugin-live-server');

export default {
    input: client,
    plugins: [liveServer({
      port: 8001,
      host: "0.0.0.0",
      root: "demo",
      file: "index.html",
      mount: [['/dist', './dist'], ['/src', './src'], ['/node_modules', './node_modules']],
      open: false,
      wait: 500
    })],
    output: {
      file: 'dist/my-fabulous-thingy.js',
      format: 'es'
    }
}
```

# How it works
rollup-plugin-live-server serves the root directory and its subdirectories. It also watches the files for changes and when they happen, it sends a message through a web socket connection to the browser instructing it to reload. In order for the client side to support this, the server injects a small piece of JavaScript code to each requested html file. rollup-plugin-live-server establishes the web socket connection and listens to the reload requests.

# FAQ
- Q. Does it support HTTPS?
- A. Yes, see the live-server docs
- Q. Does it support XYZ?
- A. If live-server supports it then yes. And if live-server doesn't support it, go help live-server support it.
